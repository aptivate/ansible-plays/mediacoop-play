[![pipeline status](https://git.coop/aptivate/ansible-plays/mediacoop-play/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-plays/mediacoop-play/commits/master)

# mediacoop-play

The Media co-op plays.

## Get Started

Install the Python dependencies with:

```bash
$ pipenv install --dev
```

And then retrieve the Ansible dependencies with:

```bash
$ pipenv run ansible-galaxy install
```

Configure your environment with:

```bash
$ mv .env.example .env
```

You may need to edit this file. It is loaded automatically by Pipenv.
